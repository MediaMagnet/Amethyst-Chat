#!/usr/bin/env ruby
# frozen_string_literal: true

require 'eventmachine'
require 'configatron'
require 'websocket-eventmachine-client'
require 'restclient'
require 'oj'
require_relative 'config.rb'

# shut up rubocop
# rubocop:disable Metrics/BlockLength, Naming/MethodName, Method/MethodLength
# rubocop:disable Metrics/LineLength, Metrics/AbcSize

# look a comment
class GetUserList
  def initialize(channel)
    @channel = channel
  end

  def self.users(channel)
    userlist = []
    getchan = RestClient.get 'https://tmi.twitch.tv/group/user/' + channel + '/chatters'
    userary = Oj.load(getchan).fetch('chatters')
    userary.keys.each { |k| userary[k].each { |user| userlist.push(user) } }
    userlist
  end
end

EM.run do
  ws = WebSocket::EventMachine::Client.connect(host: 'irc-ws.chat.twitch.tv', port: 80, ssl: false)

  ws.onopen do
    puts 'Connected'
    ws.send 'CAP REQ :twitch.tv/tags twitch.tv/commands twitch.tv/membership'
    ws.send "PASS #{configatron.twitch.oauth}"
    ws.send "NICK #{configatron.irc.nick}"
    ws.send "JOIN #{configatron.twitch.irc}"

    ws.send "PRIVMSG #{configatron.twitch.irc} :Lali-Ho!"
    system('clear')
  end

  ws.onmessage do |msg, type|
    if msg.include?('PING') == true
      ws.send 'PONG :tmi.twitch.tv'

    elsif msg.include?(' PRIVMSG ')
      metadata = msg.split(' ')[0]
      user_msg_arr = msg.split(' ')
      user_msg_arr.shift
      user_msg_arr.shift
      user_msg_arr.shift
      user_msg_arr.shift
      user_msg_arr[0] = user_msg_arr[0].delete_prefix(':')
      user_name = metadata.split(';').keep_if { |name| name =~ /display-name=([a-zA-Z\d\W]{1,32})/ }.last.split('=').last
      if user_msg_arr.to_s.include?('\\x01ACTION')
        puts '* ' + user_name + ' ' + user_msg_arr[1..-1].join(', ').to_s.gsub(', ', ' ')
      else
        puts user_name + ': ' + user_msg_arr[0..-1].join(', ').to_s.gsub(', ', ' ')
      end

    elsif msg.include?(' JOIN ') || msg.include?(' PART ')

    else
      puts 'Epic bot sax montage'
      system('clear')
    end
  end

  ws.onclose do |code, reason|
    puts "Disconnected with status code: #{code} #{reason}"
  end

  ws.onerror do |error|
    puts "Error: #{error}"
  end

  EventMachine.next_tick do
    puts 'tick!'
  end
end

# ok rubocop you can talk again
# rubocop:enable Metrics/BlockLength, Metrics/AbcSize
# rubocop:enable Naming/MethodName, Method/MethodLength
# rubocop:enable Metrics/LineLength
