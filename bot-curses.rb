#!/usr/bin/env ruby
# frozen_string_literal: true

require 'eventmachine'
require 'configatron'
require 'faye/websocket'
require 'restclient'
require 'paint'
require 'securerandom'
require 'oj'
require 'pry'
require_relative 'config.rb'
require_relative 'IRCMessage'

require 'curses'

# shut up rubocop
# rubocop:disable Metrics/BlockLength
# rubocop:disable Metrics/LineLength

# look a comment
class GetUserList
  def initialize(channel)
    @channel = channel
  end

  def self.users(channel)
    userlist = []
    getchan = RestClient.get 'https://tmi.twitch.tv/group/user/' + channel + '/chatters'
    userary = Oj.load(getchan).fetch('chatters')
    userary.keys.each { |k| userary[k].each { |user| userlist.push(user) } }
    userlist
  end
end

$chat = nil

Curses.init_screen
begin
  $x = Curses.cols
  $y = Curses.lines
  $chat = Curses::Window.new($y, $x*0.8, 0, 0)
  # $chat.box('|', '-')
  $chat.setscrreg(0, 0)
  $chat.scrollok(true)
  $chat.setpos(1,1)
  # $chat.setpos($y-2, 1)
  $chat.addstr('Hello')
  userlist = Curses::Window.new($y, $x*0.2, 0, $x*0.8)
  userlist.box('"', '=')
  userlist.setpos(1, 1)
  userlist.addstr('User List')
  $chat.refresh
  userlist.refresh

  def draw(str)
    $chat.setpos($chat.cury+1, 1)
    $chat.addstr(str)
    # $chat.scrl(1)
    $chat.refresh
  end

  t1 = Thread.new do
    EM.run {
      ws = Faye::WebSocket::Client.new('ws://irc-ws.chat.twitch.tv')

      ws.on :open do |event|
        draw(['Connected'].to_s)
        ws.send('CAP REQ :twitch.tv/tags twitch.tv/commands twitch.tv/membership')
        ws.send("PASS #{configatron.twitch.oauth}")
        ws.send("NICK #{configatron.irc.nick}")
        ws.send("JOIN #{configatron.twitch.irc}")

        # ws.send("PRIVMSG #{configatron.twitch.irc} :Lali-Ho!")
        # system('clear')
      end

      ws.on :message do |event|
        msg1 = event.data
        msg = TwitchMessage.new(msg1)

        if msg.command == 'PING'
          ws.send('PONG :tmi.twitch.tv')

        elsif msg.command == 'PRIVMSG'

          if msg.broadcaster?
            user_symbol = "\u2707"
            user_color = :red
          elsif msg.vip?
            user_symbol = "\u273E"
            user_color = :blue
          elsif msg.moderator?
            user_symbol = "\u27C1"
            user_color = :green
          else
            user_symbol = ''
            user_color = :purple
          end
=begin
          if msg.color == ''
            user_color = SecureRandom.hex(3)
          else
            user_color = msg.color
          end
=end
          if msg.message.include?('ACTION')
            user_em = msg.message.gsub('ACTION', '')
            draw(Paint['* ', :white, :yellow] + Paint["\uE0B0 ", :yellow, user_color] + Paint[user_symbol + msg.tags['display-name'] + ' ', :white, user_color] + Paint["\uE0B0\uE0B1", user_color] + user_em)
          else
            draw(Paint[user_symbol + msg.tags['display-name'] + ': ', :white, user_color] + Paint["\uE0B0\uE0B1", user_color] + msg.message)
          end

        elsif msg.command == 'JOIN' || msg.command == 'PART'

        else
          # puts event.data
          # system('clear')
        end
      end

      ws.on :close do |event|
        draw("Disconnected with status code: #{event.code} #{event.reason} #{event}")
      end

      ws.on :error do |event|
        draw("Error: #{event.error}")
      end

      EventMachine.next_tick do
        draw('tick!')
      end
    }
  end

  t1.join

ensure
  Curses.close_screen
end
# ok rubocop you can talk again
# rubocop:enable Metrics/BlockLength, Style/UnneededInterpolation
# rubocop:enable Metrics/LineLength
