#!/usr/bin/env ruby
# frozen_string_literal: true

require 'eventmachine'
require 'configatron'
require 'faye/websocket'
require 'paint'
require 'securerandom'
require 'oj'
require 'slop'
require 'sugilite'
require 'tty-screen'
require 'word_wrap'
require 'word_wrap/core_ext'

# required files
require_relative 'config'

# shut up rubocop
# rubocop:disable Metrics/BlockLength
# rubocop:disable Metrics/LineLength

helptext = "Welcome to Amethyst Chat
  -u, --user = user you want to connect to
  -d, --debug = extra output"

# No channel error
class NoChannel < StandardError
  def initialize(msg = 'You must specify a channel with -u or -user', opt = 'user')
    @opt = opt
    super(msg)
  end
end

opts = Slop.parse do |o|
  o.on '-h', '--help', 'help' do
    puts helptext.to_s
    exit
  end
  o.string '-u', '--user', 'Username'
  o.bool '-d', '--debug', 'debug output'
end
if opts[:user].nil?
  raise NoChannel.new('You must specify a channel with -u or -user', 'user')
end

@user = opts[:user]
@debug = opts[:debug]

puts @user

t1 = Thread.new do
  EM.run do
    ws = Faye::WebSocket::Client.new('ws://irc-ws.chat.twitch.tv')

    ws.on :open do |event|
      p event
      p ['Connected']
      ws.send('CAP REQ :twitch.tv/tags twitch.tv/commands twitch.tv/membership')
      ws.send("PASS #{configatron.twitch.oauth}")
      ws.send("NICK #{configatron.irc.nick}")
      ws.send("JOIN ##{@user}")

      # ws.send("PRIVMSG #{configatron.twitch.irc} :Lali-Ho!")
      system('clear')
    end

    ws.on :message do |event|
      msg1 = event.data
      msg = SugiliteIRC::TwitchMessage.new(msg1)

      if msg.command == 'PING'
        ws.send('PONG :tmi.twitch.tv')

      elsif msg.command == 'PRIVMSG'

        user_symbol = if msg.broadcaster?
                        "\uf03d "
                      elsif msg.vip?
                        "󰦤 "# "\udb82"+"\udda4"
                      elsif msg.moderator?
                        "󰓥 "# "\udb81"+"\udce5"
                      else
                        ''
                      end

        user_color = if msg.color == ''
                       SecureRandom.hex(3)
                     else
                       msg.color
                     end
        action_msg = Paint['󰿎 ', :white, '#00aaaa'] + Paint["\uE0B0 ", '#00aaaa', user_color] + Paint[user_symbol + msg.tags['display-name'] + ' ', :white, user_color] + Paint["\uE0B0\uE0B1", user_color]
        normal_msg = Paint[user_symbol + msg.tags['display-name'] + ': ', :white, user_color] + Paint["\uE0B0\uE0B1", user_color]

        if msg.message.include?('ACTION')
          user_em = msg.message.gsub('ACTION', '')
          puts event.data if @debug == true
          puts action_msg + "\n" + user_em.wrap(TTY::Screen.cols)
        else
          puts event.data if @debug == true
          puts normal_msg + "\n" + msg.message.wrap(TTY::Screen.cols)
        end

      elsif msg.command == 'JOIN' || msg.command == 'PART'

      else
        puts event.data if @debug == true
        # system('clear')
      end
    end

    ws.on :close do |event|
      puts "Disconnected with status code: #{event.code} #{event.reason} #{event}"
    end

    ws.on :error do |event|
      puts "Error: #{event.error}"
    end

    EventMachine.next_tick do
      puts 'tick!'
    end
  end
end

t1.join
# ok rubocop you can talk again
# rubocop:enable Metrics/BlockLength
# rubocop:enable Metrics/LineLength
